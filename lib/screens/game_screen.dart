import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'controller/home_controller.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<GameScreen> {
  final _homeController = HomeController();
  late final StreamSubscription _streamSubscription;

  Timer? _timer;
  Timer? _networkImageUrlTimer;
  int timerValue = 30;

  String imageUrl = "https://picsum.photos/200/300";

  @override
  void initState() {
    _startTimer();
    _initStreamObserver();
    _initImagesTimer();
  }

  void _startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (timerValue == 0) {
          _timer?.cancel();
          _navigateToSuccess();
        } else {
          setState(
            () {
              timerValue--;
            },
          );
        }
      },
    );
  }

  void _initImagesTimer() {
    const oneSec = Duration(seconds: 1);
    _networkImageUrlTimer = Timer.periodic(
      oneSec,
      (Timer timer) {
        setState(() {
          imageUrl = 'https://picsum.photos/200/300?${timer.tick}';
        });
      },
    );
  }

  void _initStreamObserver() {
    _streamSubscription =
        _homeController.gameOverStream.stream.listen((isGameOver) {
      if (isGameOver) {
        _navigateToFailure(context);
      }
    });
  }

  void _navigateToSuccess() {
    _clearSubscriptions();
    Future.delayed(Duration.zero, () {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/success', (route) => false);
    });
  }

  void _navigateToFailure(BuildContext context) {
    _clearSubscriptions();
    Navigator.of(context).pushNamedAndRemoveUntil('/failure', (route) => false);
  }

  @override
  void dispose() {
    _clearSubscriptions();
    super.dispose();
  }

  void _clearSubscriptions() {
    _timer?.cancel();
    _streamSubscription.cancel();
    _networkImageUrlTimer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<HomeController>(
        init: _homeController,
        initState: (_) async {
          await _homeController.loadCamera();
          _homeController.startImageStream();
        },
        builder: (_) {
          return SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 60,
                vertical: 15,
              ),
              child: Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Align(
                          child: SvgPicture.asset('assets/timer_bg.svg'),
                        ),
                        Align(
                          child: Text(
                            timerValue.toString(),
                            style: const TextStyle(
                              fontSize: 48,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // _.cameraController != null &&
                  //         _.cameraController!.value.isInitialized
                  //     ? Container(
                  //         alignment: Alignment.center,
                  //         width: MediaQuery.of(context).size.width,
                  //         height: MediaQuery.of(context).size.height * 0.5,
                  //         child: CameraPreview(_.cameraController!))
                  //     : const Center(child: Text('loading')),
                  const SizedBox(height: 15),
                  Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.topCenter,
                      width: 200,
                      height: 300,
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(25.0)),
                        child: Image.network(
                          imageUrl,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    '${_.label}',
                    style: const TextStyle(
                      fontSize: 18,
                      color: Colors.red,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
