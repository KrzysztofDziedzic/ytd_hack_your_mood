import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:liquid_swipe/liquid_swipe.dart';
import 'package:ytd_hack_your_mood/screens/pages_onboarding.dart';

class OnBoarding extends StatefulWidget {
  static const style = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w600, color: Color(0xffF3675F));

  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoarding createState() => _OnBoarding();
}

class _OnBoarding extends State<OnBoarding> {
  int page = 0;
  var label = "Następna";
  late LiquidController liquidController;
  late UpdateType updateType;

  @override
  void initState() {
    liquidController = LiquidController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              LiquidSwipe(
                pages: pages,
                waveType: WaveType.circularReveal,
                liquidController: liquidController,
                enableSideReveal: true,
                enableLoop: false,
                ignoreUserGestureWhileAnimating: true,
              ),
              TextButton(
                style: ElevatedButton.styleFrom(
                    splashFactory: NoSplash.splashFactory),
                onPressed: () {
                  if (page + 1 > 2) {
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil('/menu', (route) => false);
                  } else if (page < 2) {
                    page = page + 1;
                    setState(() {
                      if (page == 2) {
                        label = "Jest sztos";
                      } else {
                        label = "Następna";
                      }
                    });
                    liquidController.animateToPage(page: page);
                  }
                },
                child: Transform.translate(
                  offset: const Offset(40.0, 0.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: SizedBox(
                      width: 220,
                      height: 180,
                      child: Stack(
                        children: [
                          Transform.rotate(
                              angle: pi * -0.8,
                              child: SvgPicture.asset(
                                  'assets/blob_onboarding.svg')),
                          Center(
                            child: Transform.translate(
                              offset: const Offset(-4.0, -7.0),
                              child: Text(
                                label,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
