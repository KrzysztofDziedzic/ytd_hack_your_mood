import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer? _timer;
  int timerValue = 2;

  @override
  void initState() {
    _startTimer();
  }

  void _startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (timerValue == 0) {
          _timer?.cancel();
          Future.delayed(Duration.zero, () {
            Navigator.of(context)
                .pushNamedAndRemoveUntil('/onboarding', (route) => false);
          });
        } else {
          setState(
                () {
              timerValue--;
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: Stack(
          children: [
            Align(
              child: Padding(
                padding: const EdgeInsets.all(48.0),
                child: Image.asset('assets/logo.png'),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset('assets/eu_banner_1.jpeg'),
            ),
          ],
        ),
      ),
    );
  }
}
