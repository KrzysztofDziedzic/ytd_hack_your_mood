import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SuccessScreen extends StatelessWidget {
  const SuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 60,
            vertical: 32,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 60,
              ),
              const Text(
                'Czy to ptak?',
                style: TextStyle(
                  color: Color(0xFFBE1E2D),
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Text(
                'Czy to samolot?',
                style: TextStyle(
                  color: Color(0xFFBE1E2D),
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              const Text(
                'WYGRANA',
                style: TextStyle(
                  color: Color(0xFFF6921E),
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: SvgPicture.asset('assets/undraw_winners_re_wr1l.svg'),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Poka mi punkty',
                    style: TextStyle(
                      color: Color(0xFFF6921E),
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    width: 100,
                    height: 80,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed('/points');
                      },
                      child: Stack(
                        children: [
                          SvgPicture.asset('assets/blob_next.svg'),
                          Align(
                            alignment: Alignment.center,
                            child: SvgPicture.asset(
                              'assets/arrow_right.svg',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
