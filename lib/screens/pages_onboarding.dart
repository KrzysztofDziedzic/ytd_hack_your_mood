import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'onboarding.dart';

final pages = [
  Container(
    color: Colors.white,
    child: Padding(
      padding: const EdgeInsets.only(
          left: 16.0, right: 16.0, top: 32.0, bottom: 130.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(
            height: 300,
            child: SvgPicture.asset(
              'assets/undraw_with_love_re_1q3m.svg',
              fit: BoxFit.scaleDown,
            ),
          ),
          Column(
            children: const <Widget>[
              Text(
                "Spraw sobie trochę radości...",
                style: OnBoarding.style,
              ),
            ],
          ),
        ],
      ),
    ),
  ),
  Container(
    color: Colors.white,
    child: Padding(
      padding: const EdgeInsets.only(
          left: 16.0, right: 16.0, top: 32.0, bottom: 130.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(
            height: 300,
            child: SvgPicture.asset(
              'assets/undraw_circuit_board_4c4d.svg',
              fit: BoxFit.scaleDown,
            ),
          ),
          const Text(
            "Wprowadź swój umysł w\nniesamowity stan pozwalający\ndoświadczyć wiele...",
            style: OnBoarding.style,
          ),
        ],
      ),
    ),
  ),
  Container(
    color: Colors.white,
    child: Padding(
      padding: const EdgeInsets.only(
          left: 16.0, right: 16.0, top: 32.0, bottom: 140.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(
            height: 240,
            child: SvgPicture.asset(
              'assets/undraw_game_day_ucx9.svg',
              fit: BoxFit.scaleDown,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Wraz z",
                style: OnBoarding.style,
              ),
              Image.asset(
                'assets/wordart.png',
                height: 200,
                width: 200,
              ),
              const Text(
                "dzień staje się lepszy!",
                style: OnBoarding.style,
              ),
            ],
          )
        ],
      ),
    ),
  ),
];
