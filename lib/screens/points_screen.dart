import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ytd_hack_your_mood/common/button.dart';

class PointsScreen extends StatelessWidget {
  const PointsScreen({Key? key}) : super(key: key);

  int _getPoints() {
    var rng = Random();
    return rng.nextInt(100);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 60,
            vertical: 32,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 60,
              ),
              const Text(
                'Hey You!',
                style: TextStyle(
                  color: Color(0xFFBE1E2D),
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Text(
                'Zdobyłeś MASĘ,\na możę TONĘ\na nawet RZESZĘ\noczywiście punktów',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFF6921E),
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              SizedBox(
                height: 260,
                child: Stack(
                  children: [
                    Expanded(
                      child: SvgPicture.asset(
                          'assets/undraw_screen_time_vkev.svg'),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 64,
                        horizontal: 32,
                      ),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          'aż',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xFFF6921E),
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 64,
                      left: 32,
                      child: Text(
                        '${_getPoints()} ptk',
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          color: Color(0xFFF6921E),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              PrimaryButton(
                onClick: () {
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil('/menu', (route) => false);
                },
                label: 'Ja chcę jeszcze raz!',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
