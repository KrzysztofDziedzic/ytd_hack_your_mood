import 'package:flutter/material.dart';
import 'package:ytd_hack_your_mood/screens/splash_screen.dart';
import 'package:ytd_hack_your_mood/screens/game_screen.dart';
import 'package:ytd_hack_your_mood/screens/main_menu_screen.dart';
import 'package:ytd_hack_your_mood/screens/failure_screen.dart';
import 'package:ytd_hack_your_mood/screens/onboarding.dart';
import 'package:ytd_hack_your_mood/screens/points_screen.dart';
import 'package:ytd_hack_your_mood/screens/success_screen.dart';
import 'package:ytd_hack_your_mood/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hack Your Mood',
      theme: createThemeData(context),
      initialRoute: '/',
      routes: {
        '/': (context) => const SplashScreen(),
        '/onboarding': (context) => const OnBoarding(),
        '/menu': (context) => const MainMenuScreen(),
        '/game': (context) => const GameScreen(),
        '/failure': (context) => const FailureScreen(),
        '/success': (context) => const SuccessScreen(),
        '/points': (context) => const PointsScreen(),
      },
    );
  }
}
