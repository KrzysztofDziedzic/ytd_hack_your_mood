import 'package:flutter/material.dart';

ThemeData createThemeData(BuildContext context) => ThemeData(
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: TextButton.styleFrom(
          backgroundColor: const Color(0xFFF6921E),
        ),
      ),
    );
