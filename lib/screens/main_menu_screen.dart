import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ytd_hack_your_mood/common/button.dart';

class MainMenuScreen extends StatelessWidget {
  const MainMenuScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: SvgPicture.asset('assets/undraw_smiley_face_re_9uid.svg'),
          ),
          const Expanded(
            child: BottomSection(),
          ),
        ],
      ),
    );
  }
}

class BottomSection extends StatelessWidget {
  const BottomSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 28,
        vertical: 32,
      ),
      child: Column(
        children: [
          const Text(
            'Rozpocznij swoją grę',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 42,
          ),
          RichText(
            text: const TextSpan(
                text: 'Gra polega na nie zaśmianiu się. Masz ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.normal),
                children: <TextSpan>[
                  TextSpan(
                      text: '30',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(
                          text:
                              ' sekund by osiągnąć cel. Pamiętaj że śmiech to zdrowie więc się nie śmiej za często bo przegarsz.',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.normal),
                        )
                      ])
                ]),
          ),
          // const Text(
          //   'Gra polega na nie zaśmianiu się. Masz 30 sekund by osiągnąć cel. Pamiętaj że śmiech to zdrowie więc się nie śmiej za często bo przegarsz.',
          //   textAlign: TextAlign.center,
          //   style: TextStyle(
          //     fontSize: 16,
          //   ),
          // ),
          const Spacer(),
          PrimaryButton(
            label: 'Czas na grę!',
            onClick: () {
              Navigator.of(context).pushNamed('/game');
            },
          )
        ],
      ),
    );
  }
}
