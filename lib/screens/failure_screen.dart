import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:ytd_hack_your_mood/common/button.dart';

class FailureScreen extends StatelessWidget {
  const FailureScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 60,
            vertical: 32,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 60,
              ),
              const Text(
                'Ups!',
                style: TextStyle(
                  color: Color(0xFFBE1E2D),
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Text(
                'Niespodziewanie nastąpił koniec gry, spowodowany...',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFF6921E),
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              const Text(
                'Uśmiechem!',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xFFF6921E),
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 300,
                child: Expanded(
                  child: Lottie.asset(
                    'assets/smiley_anim.json',
                  ),
                ),
              ),
              const Spacer(),
              PrimaryButton(
                onClick: () {
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil('/menu', (route) => false);
                },
                label: 'Ja chcę jeszcze raz!',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
